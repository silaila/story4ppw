from django.contrib import admin
from django.urls import path
from .views import booksearch, data

urlpatterns = [
    path('', booksearch, name="booksearch"),
    path('data/', data, name="data"),
]
