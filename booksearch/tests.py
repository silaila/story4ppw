from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .views import booksearch, data 
from .apps import BooksearchConfig

# Create your tests here.

class Stroy8Test(TestCase) :
    def setUp(self):
        self.client = Client()
        self.booksearch = reverse("booksearch")

    def test_status_code_and_booksearch(self): 
        response = self.client.get(self.booksearch)
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_data_json_tersedia(self):
        response = Client().get("/booksearch/data/")
        self.assertEqual(response.status_code, 200)

    def test_template_used_booksearch(self): 
        response = self.client.get(self.booksearch)
        self.assertTemplateUsed(response, 'booksearch.html')

    def test_function_used_by_booksearch(self) :
        found = resolve(self.booksearch)
        self.assertEqual(found.func, booksearch)

    def test_function_used_by_json_func(self): 
        found = resolve("/booksearch/data/")
        self.assertEqual(found.func, data)

    def test_apps_is_true(self) :
        self.assertEqual(BooksearchConfig.name, 'booksearch')
        self.assertEqual(apps.get_app_config('booksearch').name, 'booksearch')