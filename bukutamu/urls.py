from django.urls import path, include 
from . import views
from .views import addactivity, listactivity, register, deleteUser

urlpatterns = [
        path('add/', addactivity, name="addactivity"),
        path('list/', listactivity, name="listactivity"),
        path('register/<int:task_id>/', register, name='register'),
        path('delete/<int:delete_id>/', deleteUser, name='delete'),

]
