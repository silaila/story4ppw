from django.shortcuts import render, redirect

# Create your views here.
from .forms import FormKegiatan, FormOrang
from .models import Kegiatan, Orang
from datetime import datetime

def addactivity(request):
    form_kegiatan = FormKegiatan()
    if request.method == "POST":
        form_kegiatan_input = FormKegiatan(request.POST)
        if form_kegiatan_input.is_valid():
            data = form_kegiatan_input.cleaned_data
            kegiatan_input = Kegiatan()
            kegiatan_input.nama_kegiatan = data['nama_kegiatan']
            kegiatan_input.deskripsi_kegiatan = data['deskripsi_kegiatan']
            kegiatan_input.save()
            # current_data = Kegiatan.objects.all()
            return redirect('/activity/list/')
            # return render(request, 'story6/addactivity.html', {'form': form_kegiatan, 'status': 'success'})
        else:
            # current_data = Kegiatan.objects.all()
            return render(request, 'addactivity.html', {'form': form_kegiatan, 'status': 'failed'})
    else:
        # current_data = Kegiatan.objects.all()
        return render(request, 'addactivity.html', {'form': form_kegiatan})


def listactivity(request):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    return render(request, 'listactivity.html', {'kegiatan': kegiatan, 'orang': orang})

def register(request, task_id):
    form_orang = FormOrang()

    if request.method == "POST":
        form_orang_input = FormOrang(request.POST)
        # print('oi')
        if form_orang_input.is_valid():
            data = form_orang_input.cleaned_data
            orangBaru = Orang()
            orangBaru.nama_orang = data['nama_orang']
            orangBaru.kegiatan = Kegiatan.objects.get(id=task_id)
            orangBaru.save()
            return redirect('/activity/list')
        else:
            return render(request, 'register.html', {'form': form_orang, 'status': 'failed'})
    else:
        return render(request, 'register.html', {'form': form_orang})

def deleteUser(request, delete_id):
    # if request.method == "POST":
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    print(delete_id)
    orang_to_delete = Orang.objects.get(id=delete_id)
    orang_to_delete.delete()
    # return render(request, 'story6/listActivity.html', {'kegiatan': kegiatan, 'orang': orang})
    return redirect('/activity/list')



