from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import logIn, signUp, logOut
from .apps import AccountsConfig
from .forms import LoginForm, SignUpForm
# Create your tests here.

class Story9Test(TestCase) :
    def setUp(self) :
        self.client = Client()
        self.login = reverse("login")
        self.signup = reverse("signup")
        self.logout = reverse("logout")

    def test_status_code_and_template_login(self): 
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_status_code_and_template_signup(self): 
        response = self.client.get(self.signup)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_status_code_and_logout(self): 
        response = self.client.get(self.logout)
        self.assertEqual(response.status_code, 302)

    def test_function_used_by_login(self) :
        found = resolve(self.login)
        self.assertEqual(found.func, logIn)

    def test_function_used_by_signup(self) :
        found = resolve(self.signup)
        self.assertEqual(found.func, signUp)

    def test_function_used_by_logout(self) :
        found = resolve(self.logout)
        self.assertEqual(found.func, logOut)    
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_login_test(self):
        response = self.client.post('/account/login/', data={'username' : 'pooh', 'password' : 'po1234oh'})
        self.assertEqual(response.status_code,200)
    
    # def test_register_test(self):
        # response = self.client.post('/account/signup/', data={'username' : 'nanda12', 'email':'nanda', 'password' : 'nanda121212'})
        # self.assertEqual(response.status_code,302)

    def test_register_already_exist_test(self):
        response = self.client.post('/account/signup/', data={'first_name': "Pooh",
            'last_name': "Bear",
            'username': "poohabear",
            'password1': "po1234oh",
            'password2': "po1234oh"})
        self.assertEqual(response.status_code,302)


    # def test_form_is_valid(self):
    #     form_login = LoginForm(data={
    #         "username": "pooh",
    #         "password": "po1234oh"
    #     })
    #     self.assertTrue(form_login.is_valid())
    #     form_signup = SignUpForm(data={
    #         'first_name': "Pooh",
    #         'last_name': "Bear",
    #         'username': "poohabear",
    #         'password1': "po1234oh",
    #         'password2': "po1234oh"
    #     })
    #     self.assertTrue(form_signup.is_valid())

    # def test_form_invalid(self):
    #     form_login = LoginForm(data={})
    #     self.assertFalse(form_login.is_valid())
    #     form_signup = SignUpForm(data={})
    #     self.assertFalse(form_signup.is_valid())    
    
    # def test_signin_header(self):
    #     request = HttpRequest()
    #     response = logIn(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertContains(response, "Sign in")
        
    # def test_signup_header(self):
    #     response = Client().get('/signup/')
    #     self.assertContains(response, "Create a new account")

    # def test_apps_is_true(self) :
    #     self.assertEqual(AccountsConfig.name, 'accounts')
    #     self.assertEqual(apps.get_app_config('accounts').name, 'accounts')

