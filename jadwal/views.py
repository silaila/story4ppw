from django.shortcuts import render, redirect 
from .models import Jadwalnya
from .forms import FormJadwal

# Create your views here.
def jadwal_form(request) :
    if request.method == "POST":
        form = FormJadwal(request.POST)
        if (form.is_valid()):
            jadwal = Jadwalnya()
            jadwal.nama = form.cleaned_data['nama']
            jadwal.dosen = form.cleaned_data['dosen']
            jadwal.sks = form.cleaned_data['sks']
            jadwal.deskripsi = form.cleaned_data['deskripsi']
            jadwal.semester = form.cleaned_data['semester']
            jadwal.ruang = form.cleaned_data['ruang']
            jadwal.save()
            return redirect('lihatjadwal/')
        return render(request, 'jadwal.html', {'form' : form})
    form = FormJadwal()
    return render(request, "jadwal.html", {"form" : form})


def lihat_jadwal(request):
    jadwal = Jadwalnya.objects.all()
    response = {
        'jadwal' : jadwal,
    }
    return render(request,'lihatjadwal.html', response)

def delete_jadwal(request, id):
        Jadwalnya.objects.filter(id=id).delete()
    # try :
    #     deleted_jadwal = Jadwalnya.objects.get(pk = id)
    #     delete_jadwal.delete()
    #     return redirect('lihatjadwal/')
        
        return render(request, 'lihatjadwal.html')
    # except :
    #     return redirect('lihatjadwal/')
    #     return render(request, 'lihatjadwal.html')