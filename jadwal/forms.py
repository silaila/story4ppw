from django import forms

class FormJadwal(forms.Form):
     nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Mata Kuliah',
        'type' : 'text',
        'required' : True
    }))

     dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Dosen',
        'type' : 'text',
        'required' : True
    }))

     sks = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'text',
        'required' : True
    }))

     deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Deskripsikan mata kuliahmu',
        'type' : 'text',
        'required' : True
    }))

     semester = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Semester Tahun',
        'type' : 'text',
        'required' : True
    }))

     ruang = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Ruang Kuliah',
        'type' : 'text',
        'required' : True
    }))






    