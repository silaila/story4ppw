from django.db import models

# Create your models here.
class Jadwalnya(models.Model) :
    nama = models.CharField(max_length=100, blank=False)
    dosen = models.CharField(max_length=100, blank=False)
    sks = models.CharField(max_length=100, blank=False)
    deskripsi = models.CharField(max_length=100, blank=False)
    semester = models.CharField(max_length=100, blank=False)
    ruang = models.CharField(max_length=100, blank=False)
