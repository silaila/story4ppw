from django.urls import path, include 
from .views import jadwal_form, lihat_jadwal, delete_jadwal

urlpatterns = [
    path('', jadwal_form, name="jadwal_form"),
    path('lihatjadwal/', lihat_jadwal, name="lihat_jadwal"),
    path('lihatjadwal/<int:id>', delete_jadwal, name="delete_jadwal")
    # path('delete/P<int:delete_id>/', delete_jadwal , name='delete'),
]