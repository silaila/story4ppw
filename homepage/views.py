from django.shortcuts import render

# Create your views here.

def index(request) :
    return render(request, "homepage.html")

def resume(request) :
    return render(request, "resume.html")

def tulisan(request) :
    return render(request, "tulisan.html")

def webproject(request) :
    return render(request, "webproject.html")