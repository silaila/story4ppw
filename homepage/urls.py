from django.urls import path, include 
from .views import index, resume, tulisan, webproject

urlpatterns = [
    path('', index, name="home"),
    path('resume', resume, name="profil"),
    # path('rekomendasi', tulisan, name="rekomendasi"),
    path('webproject', webproject, name="webproject"),
]