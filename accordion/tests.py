from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from .views import accordion
from .apps import AccordionConfig

# Create your tests here.

class Stroy7Test(TestCase) :
    def setUp(self):
        self.client = Client()
        self.accordion = reverse("accordion")

    def test_status_code_and_accordion(self): 
        response = self.client.get(self.accordion)
        self.assertEqual(response.status_code, 200)

    def test_template_used_accordion(self): 
        response = self.client.get(self.accordion)
        self.assertTemplateUsed(response, 'accordion.html')

    def test_function_used_by_accordion(self) :
        found = resolve(self.accordion)
        self.assertEqual(found.func, accordion)

    def test_apps_is_true(self) :
        self.assertEqual(AccordionConfig.name, 'accordion')
        self.assertEqual(apps.get_app_config('accordion').name, 'accordion')